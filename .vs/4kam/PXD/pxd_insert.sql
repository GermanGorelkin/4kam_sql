
INSERT into [core].[PromoPlanMechanics] (PromoPlanID,MechanicTypeID,MechanicValue)
select distinct pp.ID,8,pv.[all]
 from [core].[PromoPlan] pp
inner join Networks n on n.id = pp.NetworkID and n.active = 1
inner join source.PXD p on n.name = p.��  --and p.[������] !='Russia'
inner join [core].[PromoPlanPFormat] pppf on pppf.PromoPlanID = pp.ID
inner join KaRegions kr on kr.kaID = n.id
inner join Territorys t on t.id = kr.regionID and (t.parentID = 1645 or t.id=1645)
inner join source.PXD_val pv on pv.id = pppf.ProductFormatID and pv.[all] != '' and pv.[all] is not null
where pp.BeginDeliveryDate between '2018-05-01' and '2018-05-13' and pp.BeginShelfDate between '2018-05-01' and '2018-05-13'  and pp.IsDel = 0 --and p.PXD = 'min' 
	and pp.PromoTypeID in (2,3)

INSERT into [core].[PromoPlanMechanics] (PromoPlanID,MechanicTypeID,MechanicValue)
select distinct pp.ID,8,pv.[min]
 from [core].[PromoPlan] pp
inner join Networks n on n.id = pp.NetworkID and n.active = 1
inner join source.PXD p on n.name = p.��   -- and p.[������] !='Russia'
inner join [core].[PromoPlanPFormat] pppf on pppf.PromoPlanID = pp.ID
inner join KaRegions kr on kr.kaID = n.id
inner join Territorys t on t.id = kr.regionID and (t.parentID = 1645 or t.id=1645)
inner join source.PXD_val pv on pv.id = pppf.ProductFormatID and pv.[min] != '' and pv.[min] is not null
where pp.BeginDeliveryDate between '2018-06-01' and '2018-06-14' and pp.BeginShelfDate between '2018-06-01' and '2018-06-14'  and pp.IsDel = 0 and p.PXD = 'min' 
	and pp.PromoTypeID in (2,3)

INSERT into [core].[PromoPlanMechanics] (PromoPlanID,MechanicTypeID,MechanicValue)
select distinct pp.ID,8,pv.[all]
 from [core].[PromoPlan] pp
inner join Networks n on n.id = pp.NetworkID and n.active = 1
inner join source.PXD p on n.name = p.��   --and p.[������] !='Russia'
inner join [core].[PromoPlanPFormat] pppf on pppf.PromoPlanID = pp.ID
inner join KaRegions kr on kr.kaID = n.id
inner join Territorys t on t.id = kr.regionID and (t.parentID = 1645 or t.id=1645)
inner join source.PXD_val pv on pv.id = pppf.ProductFormatID and pv.[all] != '' and pv.[all] is not null
where pp.BeginDeliveryDate between '2018-05-01' and '2018-05-31'  and pp.IsDel = 0 and p.PXD = 'max' 
and not exists (
select ID from [core].[PromoPlanMechanics] plm
where plm.PromoPlanID = pp.ID and plm.MechanicTypeID = 8
)
	and pp.PromoTypeID in (2,3)

INSERT into [core].[PromoPlanMechanics] (PromoPlanID,MechanicTypeID,MechanicValue)
select distinct pp.ID,8,pv.[total]
 from [core].[PromoPlan] pp
inner join Networks n on n.id = pp.NetworkID and n.active = 1
inner join source.PXD p on n.name = p.��  -- and p.[������] !='Russia'
inner join [core].[PromoPlanPFormat] pppf on pppf.PromoPlanID = pp.ID
inner join KaRegions kr on kr.kaID = n.id
inner join Territorys t on t.id = kr.regionID and (t.parentID = 1645 or t.id=1645)
inner join source.PXD_val_calc pv on pv.id = pppf.ProductFormatID and pv.[total] != '' and pv.[total] is not null
where pp.BeginShelfDate between '2018-06-01' and '2018-06-14' and pp.IsDel = 0 and p.PXD = 'max' 
and not exists (
select ID from [core].[PromoPlanMechanics] plm
where plm.PromoPlanID = pp.ID and plm.MechanicTypeID = 8
)
	and pp.PromoTypeID in (2,3)
