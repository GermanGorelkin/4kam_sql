declare 
	@beginDate datetime = N'20161101'
	, @endDate datetime = N'20171201'

--Prepare
--truncate table [core].[PromoPlanProduct_temp]
--insert into [core].[PromoPlanProduct_temp](PromoPlanID, ProductID, Data, CreatedUserID, BeginDate, EndDate, DistrBranchID)
--select 
--PromoPlanID, ProductID, Data, CreatedUserID, BeginDate, EndDate, DistrBranchID
--from [core].[PromoPlanProduct]


declare @PromoPlan table
(
	PromoPlanID int
)
insert into @PromoPlan
select distinct
	ppp.PromoPlanID
from 
[core].[PromoPlanProduct] ppp
inner join [core].[PromoPlan] pp
	on ppp.PromoPlanID=pp.ID
inner join [dbo].[getWeeksByPeriod](@beginDate, @endDate) weeks
	on ppp.BeginDate between weeks.BeginDate and weeks.EndDate
		and ppp.EndDate between weeks.BeginDate and weeks.EndDate
group by ppp.PromoPlanID, ppp.ProductID, weeks.BeginDate, weeks.EndDate, ppp.DistrBranchID, pp.BeginDeliveryDate, pp.EndDeliveryDate
having count (ppp.Data)>1


delete from [core].[PromoPlanProduct]  where PromoPlanID in (select PromoPlanID from @PromoPlan) 


insert into [core].[PromoPlanProduct](PromoPlanID, ProductID, Data, CreatedUserID, BeginDate, EndDate, DistrBranchID)
select 
	ppp.PromoPlanID
	, ppp.ProductID
	, sum(ppp.Data) as Data
	, max(ppp.CreatedUserID) as CreatedUserID
	, case 
		when weeks.BeginDate<pp.BeginDeliveryDate then pp.BeginDeliveryDate
		else  weeks.BeginDate end as BeginDate
	, case
		when weeks.EndDate>pp.EndDeliveryDate then pp.EndDeliveryDate
		else weeks.EndDate end as EndDate
	, ppp.DistrBranchID
from 
[core].[PromoPlanProduct_temp] ppp
inner join [core].[PromoPlan] pp
	on ppp.PromoPlanID=pp.ID
inner join [dbo].[getWeeksByPeriod](@beginDate, @endDate) weeks
	on ppp.BeginDate between weeks.BeginDate and weeks.EndDate
		and ppp.EndDate between weeks.BeginDate and weeks.EndDate
where PromoPlanID in (select PromoPlanID from @PromoPlan) 
group by ppp.PromoPlanID, ppp.ProductID, weeks.BeginDate, weeks.EndDate, ppp.DistrBranchID, pp.BeginDeliveryDate, pp.EndDeliveryDate
having sum(ppp.Data)>0

--Test
--select sum(Data) from [core].[PromoPlanProduct]
--select sum(Data) from [core].[PromoPlanProduct_temp]