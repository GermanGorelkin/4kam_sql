--;with Filter0 as 
--(select 
--	pp1.ID as PromoPlanID
--	, pp1.BeginDeliveryDate
--	, pp1.EndDeliveryDate
--	, dateadd(day, -1, min(pp2.BeginDeliveryDate)) as NewEndDeliveryDate 
--	, datediff(day, pp1.BeginDeliveryDate, dateadd(day, -1, min(pp2.BeginDeliveryDate))) as Diff
--from 
--	[core].[PromoPlan] pp1
--	inner join [core].[PromoPlanPFormat] pf1
--		on pp1.ID= pf1.PromoPlanID and pf1.Active=1
--	inner join [core].[PromoPlanPFormat] pf2
--		on pf1.ProductFormatID=pf2.ProductFormatID and pf2.Active=1
--			and pf1.PromoPlanID!=pf2.PromoPlanID
--	inner join [core].[PromoPlan] pp2
--		on pf2.PromoPlanID=pp2.ID
--		and pp1.NetworkID=pp2.NetworkID
--		and pp1.ID!=pp2.ID
--		and pp1.BeginDeliveryDate<pp2.BeginDeliveryDate
--		--and pp1.EndDeliveryDate>pp2.EndDeliveryDate
--where
--	pp1.Active=1
--	and pp2.Active=1
--	and pp1.IsDel=0
--	and pp2.IsDel=0
--	and ((pp1.BeginDeliveryDate<=pp2.EndDeliveryDate and pp1.EndDeliveryDate>=pp2.BeginDeliveryDate))
--	and pp1.BeginDeliveryDate>N'20161001'
--	and pp2.BeginDeliveryDate>N'20161001'
--group by pp1.ID, pp1.BeginDeliveryDate, pp1.EndDeliveryDate
--having datediff(day, pp1.BeginDeliveryDate, dateadd(day, -1, min(pp2.BeginDeliveryDate)))>=0)
--update pp
--	set pp.EndDeliveryDate=f.NewEndDeliveryDate
--from
--	[core].[PromoPlan] pp
--	inner join Filter0 f
--		on pp.ID=f.PromoPlanID

--update pp1
--	set pp1.EndDeliveryDate=dateadd(day, datediff(day, pp1.BeginDeliveryDate, pp1.EndDeliveryDate)/2, pp1.BeginDeliveryDate)
--from 
--	[core].[PromoPlan] pp1
--	inner join [core].[PromoPlanPFormat] pf1
--		on pp1.ID= pf1.PromoPlanID and pf1.Active=1
--	inner join [core].[PromoPlanPFormat] pf2
--		on pf1.ProductFormatID=pf2.ProductFormatID and pf2.Active=1
--			and pf1.PromoPlanID!=pf2.PromoPlanID
--	inner join [core].[PromoPlan] pp2
--		on pf2.PromoPlanID=pp2.ID
--		and pp1.NetworkID=pp2.NetworkID
--		and pp1.ID<pp2.ID
--		and pp1.BeginDeliveryDate=pp2.BeginDeliveryDate
--		and pp1.EndDeliveryDate=pp2.EndDeliveryDate
--where
--	pp1.Active=1
--	and pp2.Active=1
--	and pp1.IsDel=0
--	and pp2.IsDel=0
--	and (pp1.BeginDeliveryDate<=pp2.EndDeliveryDate and pp1.EndDeliveryDate>=pp2.BeginDeliveryDate)
--	and (pp1.BeginDeliveryDate>N'20161001')
--	and (pp2.BeginDeliveryDate>N'20161001')

--;with Filter0 as
--(select 
--	pp1.ID as PromoPlanID
--	, pp1.BeginDeliveryDate
--	, pp1.EndDeliveryDate
--	, dateadd(day, 1, max(pp2.EndDeliveryDate)) as NewBeginDeliveryDate 
--	, datediff(day, dateadd(day, 1, max(pp2.EndDeliveryDate)), pp1.EndDeliveryDate) as Diff
--from 
--	[core].[PromoPlan] pp1
--	inner join [core].[PromoPlanPFormat] pf1
--		on pp1.ID= pf1.PromoPlanID and pf1.Active=1
--	inner join [core].[PromoPlanPFormat] pf2
--		on pf1.ProductFormatID=pf2.ProductFormatID and pf2.Active=1
--			and pf1.PromoPlanID!=pf2.PromoPlanID
--	inner join [core].[PromoPlan] pp2
--		on pf2.PromoPlanID=pp2.ID
--		and pp1.NetworkID=pp2.NetworkID
--		and pp1.ID!=pp2.ID
--		and pp1.BeginDeliveryDate=pp2.BeginDeliveryDate
--		and pp1.EndDeliveryDate>pp2.EndDeliveryDate
--where
--	pp1.Active=1
--	and pp2.Active=1
--	and pp1.IsDel=0
--	and pp2.IsDel=0
--	and (/*(pp1.BeginShelfDate<=pp2.EndShelfDate and pp1.EndShelfDate>=pp2.BeginShelfDate)
--		or*/
--	(pp1.BeginDeliveryDate<=pp2.EndDeliveryDate and pp1.EndDeliveryDate>=pp2.BeginDeliveryDate))
--	and pp1.BeginDeliveryDate>N'20161001'
--	and pp2.BeginDeliveryDate>N'20161001'
--group by pp1.ID, pp1.BeginDeliveryDate, pp1.EndDeliveryDate
--having datediff(day, dateadd(day, 1, max(pp2.EndDeliveryDate)), pp1.EndDeliveryDate)>=0)
--update pp
--	set pp.BeginDeliveryDate=f.NewBeginDeliveryDate
--from
--	[core].[PromoPlan] pp
--	inner join Filter0 f
--		on pp.ID=f.PromoPlanID

----test
--select 
--	pp1.ID as PromoPlanID
--	, pp1.BeginDeliveryDate
--	, pp1.EndDeliveryDate
--	, dateadd(day, 1, max(pp2.EndDeliveryDate)) as NewBeginDeliveryDate 
--	, datediff(day, dateadd(day, 1, max(pp2.EndDeliveryDate)), pp1.EndDeliveryDate) as Diff
--from 
--	[core].[PromoPlan] pp1
--	inner join [core].[PromoPlanPFormat] pf1
--		on pp1.ID= pf1.PromoPlanID and pf1.Active=1
--	inner join [core].[PromoPlanPFormat] pf2
--		on pf1.ProductFormatID=pf2.ProductFormatID and pf2.Active=1
--			and pf1.PromoPlanID!=pf2.PromoPlanID
--	inner join [core].[PromoPlan] pp2
--		on pf2.PromoPlanID=pp2.ID
--		and pp1.NetworkID=pp2.NetworkID
--		and pp1.ID!=pp2.ID
--		and pp1.BeginDeliveryDate=pp2.BeginDeliveryDate
--		and pp1.EndDeliveryDate>pp2.EndDeliveryDate
--where
--	pp1.Active=1
--	and pp2.Active=1
--	and pp1.IsDel=0
--	and pp2.IsDel=0
--	and (pp1.BeginDeliveryDate<=pp2.EndDeliveryDate and pp1.EndDeliveryDate>=pp2.BeginDeliveryDate)
--	and (pp1.BeginDeliveryDate>N'20161001')
--group by pp1.ID, pp1.BeginDeliveryDate, pp1.EndDeliveryDate
--having datediff(day, dateadd(day, 1, max(pp2.EndDeliveryDate)), pp1.EndDeliveryDate)>0



	--and pp1.ID=12453
--having datediff(day, dateadd(day, 1, max(pp2.EndDeliveryDate)), pp1.EndDeliveryDate)>=0


select 
	pp1.ID
	, pp1.NetworkID
	, pf1.ProductFormatID
	, pp1.BeginDeliveryDate
	, pp1.EndDeliveryDate
	, pp1.CreatedDate
	, N'|' as N'|'
	, pp2.ID
	, pp2.NetworkID
	, pf2.ProductFormatID
	, pp2.BeginDeliveryDate
	, pp2.EndDeliveryDate
	, pp2.CreatedDate
from 
	[core].[PromoPlan] pp1
	inner join [core].[PromoPlanPFormat] pf1
		on pp1.ID= pf1.PromoPlanID and pf1.Active=1
	inner join [core].[PromoPlanPFormat] pf2
		on pf1.ProductFormatID=pf2.ProductFormatID and pf2.Active=1
			and pf1.PromoPlanID!=pf2.PromoPlanID
	inner join [core].[PromoPlan] pp2
		on pf2.PromoPlanID=pp2.ID
		and pp1.NetworkID=pp2.NetworkID
		and pp1.ID!=pp2.ID
		--and pp1.BeginDeliveryDate<=pp2.BeginDeliveryDate
where
	pp1.Active=1
	and pp2.Active=1
	and pp1.IsDel=0
	and pp2.IsDel=0
	and (/*(pp1.BeginShelfDate<=pp2.EndShelfDate and pp1.EndShelfDate>=pp2.BeginShelfDate)
		or*/
	(pp1.BeginDeliveryDate<=pp2.EndDeliveryDate and pp1.EndDeliveryDate>=pp2.BeginDeliveryDate))
	and (pp1.BeginDeliveryDate>N'20161001')
	and (pp2.BeginDeliveryDate>N'20161001')
	--and pp1.ID=197899
order by pp1.ID





