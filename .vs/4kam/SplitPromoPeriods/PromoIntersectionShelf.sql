--;with Filter0 as 
--(select 
--	pp1.ID as PromoPlanID
--	, pp1.BeginShelfDate
--	, pp1.EndShelfDate
--	, dateadd(day, -1, min(pp2.BeginShelfDate)) as NewEndShelfDate 
--	, datediff(day, pp1.BeginShelfDate, dateadd(day, -1, min(pp2.BeginShelfDate))) as Diff
--from 
--	[core].[PromoPlan] pp1
--	inner join [core].[PromoPlanPFormat] pf1
--		on pp1.ID= pf1.PromoPlanID and pf1.Active=1
--	inner join [core].[PromoPlanPFormat] pf2
--		on pf1.ProductFormatID=pf2.ProductFormatID and pf2.Active=1
--			and pf1.PromoPlanID!=pf2.PromoPlanID
--	inner join [core].[PromoPlan] pp2
--		on pf2.PromoPlanID=pp2.ID
--		and pp1.NetworkID=pp2.NetworkID
--		and pp1.ID!=pp2.ID
--		and pp1.BeginShelfDate<pp2.BeginShelfDate
--		--and pp1.EndShelfDate>pp2.EndShelfDate
--where
--	pp1.Active=1
--	and pp2.Active=1
--	and pp1.IsDel=0
--	and pp2.IsDel=0
--	and ((pp1.BeginShelfDate<=pp2.EndShelfDate and pp1.EndShelfDate>=pp2.BeginShelfDate))
--	and pp1.BeginShelfDate>N'20161001'
--	and pp2.BeginShelfDate>N'20161001'
--group by pp1.ID, pp1.BeginShelfDate, pp1.EndShelfDate
--having datediff(day, pp1.BeginShelfDate, dateadd(day, -1, min(pp2.BeginShelfDate)))>=0)
--update pp
--	set pp.EndShelfDate=f.NewEndShelfDate
--from
--	[core].[PromoPlan] pp
--	inner join Filter0 f
--		on pp.ID=f.PromoPlanID

--update pp1
--	set pp1.EndShelfDate=dateadd(day, datediff(day, pp1.BeginShelfDate, pp1.EndShelfDate)/2, pp1.BeginShelfDate)
--from 
--	[core].[PromoPlan] pp1
--	inner join [core].[PromoPlanPFormat] pf1
--		on pp1.ID= pf1.PromoPlanID and pf1.Active=1
--	inner join [core].[PromoPlanPFormat] pf2
--		on pf1.ProductFormatID=pf2.ProductFormatID and pf2.Active=1
--			and pf1.PromoPlanID!=pf2.PromoPlanID
--	inner join [core].[PromoPlan] pp2
--		on pf2.PromoPlanID=pp2.ID
--		and pp1.NetworkID=pp2.NetworkID
--		and pp1.ID<pp2.ID
--		and pp1.BeginShelfDate=pp2.BeginShelfDate
--		and pp1.EndShelfDate=pp2.EndShelfDate
--where
--	pp1.Active=1
--	and pp2.Active=1
--	and pp1.IsDel=0
--	and pp2.IsDel=0
--	and (pp1.BeginShelfDate<=pp2.EndShelfDate and pp1.EndShelfDate>=pp2.BeginShelfDate)
--	and (pp1.BeginShelfDate>N'20161001')
--	and (pp2.BeginShelfDate>N'20161001')

--;with Filter0 as
--(select 
--	pp1.ID as PromoPlanID
--	, pp1.BeginShelfDate
--	, pp1.EndShelfDate
--	, dateadd(day, 1, max(pp2.EndShelfDate)) as NewBeginShelfDate 
--	, datediff(day, dateadd(day, 1, max(pp2.EndShelfDate)), pp1.EndShelfDate) as Diff
--from 
--	[core].[PromoPlan] pp1
--	inner join [core].[PromoPlanPFormat] pf1
--		on pp1.ID= pf1.PromoPlanID and pf1.Active=1
--	inner join [core].[PromoPlanPFormat] pf2
--		on pf1.ProductFormatID=pf2.ProductFormatID and pf2.Active=1
--			and pf1.PromoPlanID!=pf2.PromoPlanID
--	inner join [core].[PromoPlan] pp2
--		on pf2.PromoPlanID=pp2.ID
--		and pp1.NetworkID=pp2.NetworkID
--		and pp1.ID!=pp2.ID
--		and pp1.BeginShelfDate=pp2.BeginShelfDate
--		and pp1.EndShelfDate>pp2.EndShelfDate
--where
--	pp1.Active=1
--	and pp2.Active=1
--	and pp1.IsDel=0
--	and pp2.IsDel=0
--	and (/*(pp1.BeginShelfDate<=pp2.EndShelfDate and pp1.EndShelfDate>=pp2.BeginShelfDate)
--		or*/
--	(pp1.BeginShelfDate<=pp2.EndShelfDate and pp1.EndShelfDate>=pp2.BeginShelfDate))
--	and pp1.BeginShelfDate>N'20161001'
--	and pp2.BeginShelfDate>N'20161001'
--group by pp1.ID, pp1.BeginShelfDate, pp1.EndShelfDate
--having datediff(day, dateadd(day, 1, max(pp2.EndShelfDate)), pp1.EndShelfDate)>=0)
--update pp
--	set pp.BeginShelfDate=f.NewBeginShelfDate
--from
--	[core].[PromoPlan] pp
--	inner join Filter0 f
--		on pp.ID=f.PromoPlanID

------test
--select 
--	pp1.ID as PromoPlanID
--	, pp1.BeginShelfDate
--	, pp1.EndShelfDate
--	, dateadd(day, 1, max(pp2.EndShelfDate)) as NewBeginShelfDate 
--	, datediff(day, dateadd(day, 1, max(pp2.EndShelfDate)), pp1.EndShelfDate) as Diff
--from 
--	[core].[PromoPlan] pp1
--	inner join [core].[PromoPlanPFormat] pf1
--		on pp1.ID= pf1.PromoPlanID and pf1.Active=1
--	inner join [core].[PromoPlanPFormat] pf2
--		on pf1.ProductFormatID=pf2.ProductFormatID and pf2.Active=1
--			and pf1.PromoPlanID!=pf2.PromoPlanID
--	inner join [core].[PromoPlan] pp2
--		on pf2.PromoPlanID=pp2.ID
--		and pp1.NetworkID=pp2.NetworkID
--		and pp1.ID!=pp2.ID
--		--and pp1.BeginShelfDate=pp2.BeginShelfDate
--		--and pp1.EndShelfDate>pp2.EndShelfDate
--where
--	pp1.Active=1
--	and pp2.Active=1
--	and pp1.IsDel=0
--	and pp2.IsDel=0
--	and (pp1.BeginShelfDate<=pp2.EndShelfDate and pp1.EndShelfDate>=pp2.BeginShelfDate)
--	and (pp1.BeginShelfDate>N'20161001')
--	and (pp2.BeginShelfDate>N'20161001')
--group by pp1.ID, pp1.BeginShelfDate, pp1.EndShelfDate
--having datediff(day, dateadd(day, 1, max(pp2.EndShelfDate)), pp1.EndShelfDate)>0





	--and pp1.ID=12453
--having datediff(day, dateadd(day, 1, max(pp2.EndShelfDate)), pp1.EndShelfDate)>=0


select 
	pp1.ID
	, pp1.NetworkID
	, pf1.ProductFormatID
	, pp1.BeginShelfDate
	, pp1.EndShelfDate
	, pp1.CreatedDate
	, N'|' as N'|'
	, pp2.ID
	, pp2.NetworkID
	, pf2.ProductFormatID
	, pp2.BeginShelfDate
	, pp2.EndShelfDate
	, pp2.CreatedDate
from 
	[core].[PromoPlan] pp1
	inner join [core].[PromoPlanPFormat] pf1
		on pp1.ID= pf1.PromoPlanID and pf1.Active=1
	inner join [core].[PromoPlanPFormat] pf2
		on pf1.ProductFormatID=pf2.ProductFormatID and pf2.Active=1
			and pf1.PromoPlanID!=pf2.PromoPlanID
	inner join [core].[PromoPlan] pp2
		on pf2.PromoPlanID=pp2.ID
		and pp1.NetworkID=pp2.NetworkID
		and pp1.ID!=pp2.ID
		--and pp1.BeginShelfDate<=pp2.BeginShelfDate
where
	pp1.Active=1
	and pp2.Active=1
	and pp1.IsDel=0
	and pp2.IsDel=0
	and (/*(pp1.BeginShelfDate<=pp2.EndShelfDate and pp1.EndShelfDate>=pp2.BeginShelfDate)
		or*/
	(pp1.BeginShelfDate<=pp2.EndShelfDate and pp1.EndShelfDate>=pp2.BeginShelfDate))
	and (pp1.BeginShelfDate>N'20161001')
	and (pp2.BeginShelfDate>N'20161001')
	--and pp1.ID=197899
order by pp1.ID





