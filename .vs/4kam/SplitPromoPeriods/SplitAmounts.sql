
declare @amounts [api].[product_amount_tbltype]
declare @networkID int, @createdUserID int
declare @promo table
(
	PromoPlanID int
)

insert into @promo 
select distinct
	pp.ID
from 
	[core].[PromoPlan] pp
	inner join [core].[PromoPlanProduct]ppp
		on pp.ID = ppp.PromoPlanID
where
	(ppp.BeginDate<pp.BeginDeliveryDate 
	or ppp.EndDate>pp.EndDeliveryDate)
	and pp.BeginDeliveryDate>N'20161001'


--[core].[NoPromoPlanProduct]
DECLARE my_cursor CURSOR FOR   
select distinct
	NetworkID
from [core].[PromoPlan]
where ID in (select PromoPlanID from @promo)
  
OPEN my_cursor  
FETCH NEXT FROM my_cursor   
INTO @networkID 
  
WHILE @@FETCH_STATUS = 0  
BEGIN 
	delete from @amounts

	insert into @amounts
	select
		ROW_NUMBER() over (order by ppp.ProductID)
		, ppp.ProductID
		, pp.BeginDeliveryDate
		, pp.EndDeliveryDate
		, sum(ppp.Data)
		, ppp.PromoPlanID
	from [core].[PromoPlanProduct] ppp
		inner join [core].[PromoPlan] pp
			on ppp.PromoPlanID=pp.ID
	where NetworkID = @networkID and pp.ID in (select PromoPlanID from @promo)
	group by ppp.ProductID, pp.BeginDeliveryDate, pp.EndDeliveryDate, ppp.PromoPlanID

	delete ppp 
	from [core].[PromoPlanProduct] ppp
		inner join [core].[PromoPlan] pp
			on ppp.PromoPlanID = pp.ID
	where pp.NetworkID = @networkID and ppp.PromoPlanID in (select PromoPlanID from @promo)

	insert into [core].[PromoPlanProduct](ProductID, PromoPlanID, CreatedUserID, Data, BeginDate, EndDate, DistrBranchID)
	select
		ProductID
		, PromoPlanID
		, t1.CreatedUserID
		, Amount
		, case when BeginDate<t1.BeginDeliveryDate then t1.BeginDeliveryDate
			else BeginDate end as BeginDate
		, case when EndDate>t1.EndDeliveryDate then t1.EndDeliveryDate
			else EndDate end as EndDate
		, DistrBranchID 
	from [api].[CreateNetworkBranchAmounts_2](@networkID, @amounts) t0
	inner join [core].[PromoPlan] t1
		on t0.PromoPlanID=t1.ID
	 where Amount>0
	 order by t0.PromoPlanID, t0.ProductID, t0.BeginDate


	 --select 1
	 --where
		--(select sum(Amount) from @amounts)!=(select sum(Amount) from [api].[CreateNetworkBranchAmounts_2](@networkID, @amounts))

FETCH NEXT FROM my_cursor   
INTO @networkID
END
CLOSE my_cursor;  
DEALLOCATE my_cursor;  


