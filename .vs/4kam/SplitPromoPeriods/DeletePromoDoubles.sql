--use [4kam_promo_temp]

; with Filter0 as 
(
	select
	max(pp1.ID) as PromoPlanID
	, pp1.BeginDeliveryDate
	, pp1.EndDeliveryDate
	, pp1.NetworkID
	, pf1.ProductFormatID
	, isNull(ppm1.Billboard, 0) as Billboard
	, isNull(ppm1.Budget, 0) as Budget
	, isNull(ppm1.Discount, 0) as Discount
	, isNull(ppm1.FrontPage, 0) as FrontPage
	, isNull(ppm1.Leaflet, 0) as Leaflet
	, isNull(ppm1.SecondPlace, 0) as SecondPlace
	, isNull(ppm1.Tag, 0) as Tag
from 
	[core].[PromoPlan] pp1
	inner join [core].[PromoPlanPFormat] pf1
		on pp1.ID= pf1.PromoPlanID and pf1.Active=1
	left join [core].[PromoPlanMechanic] ppm1
		on ppm1.PromoPlanID=pp1.ID
where
	isNull(pp1.IsDel,0)=0
group by pp1.BeginDeliveryDate
	, pp1.EndDeliveryDate
	, pp1.NetworkID
	, pf1.ProductFormatID
	, isNull(ppm1.Billboard, 0)
	, isNull(ppm1.Budget, 0)
	, isNull(ppm1.Discount, 0)
	, isNull(ppm1.FrontPage, 0)
	, isNull(ppm1.Leaflet, 0)
	, isNull(ppm1.SecondPlace, 0)
	, isNull(ppm1.Tag, 0)
having count(distinct pp1.ID)>1
)
delete from [core].[PromoPlanProduct]
where PromoPlanID in 
(select distinct pp1.ID
from 
	[core].[PromoPlan] pp1
	inner join [core].[PromoPlanPFormat] pf1
		on pp1.ID= pf1.PromoPlanID and pf1.Active=1
	left join [core].[PromoPlanMechanic] ppm1
		on ppm1.PromoPlanID=pp1.ID
	inner join  Filter0 f
		on f.BeginDeliveryDate=pp1.BeginDeliveryDate
			and f.Billboard=isNull(ppm1.Billboard,0)
			and f.Budget = isNull(ppm1.Budget,0)
			and f.Discount = isNull(ppm1.Discount,0)
			and f.EndDeliveryDate=pp1.EndDeliveryDate
			and f.FrontPage=isNull(ppm1.FrontPage,0)
			and f.Leaflet=isNull(ppm1.Leaflet,0)
			and f.NetworkID=pp1.NetworkID
			and f.ProductFormatID=pf1.ProductFormatID
			and f.PromoPlanID>pp1.ID
			and f.SecondPlace=isNull(ppm1.SecondPlace,0)
			and f.Tag = isnull(ppm1.Tag,0))


; with Filter0 as 
(
	select
	max(pp1.ID) as PromoPlanID
	, pp1.BeginDeliveryDate
	, pp1.EndDeliveryDate
	, pp1.NetworkID
	, pf1.ProductFormatID
	, isNull(ppm1.Billboard, 0) as Billboard
	, isNull(ppm1.Budget, 0) as Budget
	, isNull(ppm1.Discount, 0) as Discount
	, isNull(ppm1.FrontPage, 0) as FrontPage
	, isNull(ppm1.Leaflet, 0) as Leaflet
	, isNull(ppm1.SecondPlace, 0) as SecondPlace
	, isNull(ppm1.Tag, 0) as Tag
from 
	[core].[PromoPlan] pp1
	inner join [core].[PromoPlanPFormat] pf1
		on pp1.ID= pf1.PromoPlanID and pf1.Active=1
	left join [core].[PromoPlanMechanic] ppm1
		on ppm1.PromoPlanID=pp1.ID
where
	isNull(pp1.IsDel,0)=0
group by pp1.BeginDeliveryDate
	, pp1.EndDeliveryDate
	, pp1.NetworkID
	, pf1.ProductFormatID
	, isNull(ppm1.Billboard, 0)
	, isNull(ppm1.Budget, 0)
	, isNull(ppm1.Discount, 0)
	, isNull(ppm1.FrontPage, 0)
	, isNull(ppm1.Leaflet, 0)
	, isNull(ppm1.SecondPlace, 0)
	, isNull(ppm1.Tag, 0)
having count(distinct pp1.ID)>1
)
delete pp1
from 
	[core].[PromoPlan] pp1
	inner join [core].[PromoPlanPFormat] pf1
		on pp1.ID= pf1.PromoPlanID and pf1.Active=1
	left join [core].[PromoPlanMechanic] ppm1
		on ppm1.PromoPlanID=pp1.ID
	inner join  Filter0 f
		on f.BeginDeliveryDate=pp1.BeginDeliveryDate
			and f.Billboard=isNull(ppm1.Billboard,0)
			and f.Budget = isNull(ppm1.Budget,0)
			and f.Discount = isNull(ppm1.Discount,0)
			and f.EndDeliveryDate=pp1.EndDeliveryDate
			and f.FrontPage=isNull(ppm1.FrontPage,0)
			and f.Leaflet=isNull(ppm1.Leaflet,0)
			and f.NetworkID=pp1.NetworkID
			and f.ProductFormatID=pf1.ProductFormatID
			and f.PromoPlanID>pp1.ID
			and f.SecondPlace=isNull(ppm1.SecondPlace,0)
			and f.Tag = isnull(ppm1.Tag,0)

	select
	max(pp1.ID) as PromoPlanID
	, pp1.BeginDeliveryDate
	, pp1.EndDeliveryDate
	, pp1.NetworkID
	, pf1.ProductFormatID
	, isNull(ppm1.Billboard, 0) as Billboard
	, isNull(ppm1.Budget, 0) as Budget
	, isNull(ppm1.Discount, 0) as Discount
	, isNull(ppm1.FrontPage, 0) as FrontPage
	, isNull(ppm1.Leaflet, 0) as Leaflet
	, isNull(ppm1.SecondPlace, 0) as SecondPlace
	, isNull(ppm1.Tag, 0) as Tag
from 
	[core].[PromoPlan] pp1
	inner join [core].[PromoPlanPFormat] pf1
		on pp1.ID= pf1.PromoPlanID and pf1.Active=1
	left join [core].[PromoPlanMechanic] ppm1
		on ppm1.PromoPlanID=pp1.ID
where
	isNull(pp1.IsDel,0)=0
group by pp1.BeginDeliveryDate
	, pp1.EndDeliveryDate
	, pp1.NetworkID
	, pf1.ProductFormatID
	, isNull(ppm1.Billboard, 0)
	, isNull(ppm1.Budget, 0)
	, isNull(ppm1.Discount, 0)
	, isNull(ppm1.FrontPage, 0)
	, isNull(ppm1.Leaflet, 0)
	, isNull(ppm1.SecondPlace, 0)
	, isNull(ppm1.Tag, 0)
having count(distinct pp1.ID)>1

