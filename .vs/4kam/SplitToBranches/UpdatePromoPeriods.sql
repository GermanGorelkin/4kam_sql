---Объединение объемов у разбитых периодов справа
--update ppp
--	set ppp.Data = ppp.Data+ppp1.Data+isNull(ppp2.Data,0)+isNull(ppp3.Data,0)+isNull(ppp4.Data,0) as Data
select distinct
	ppp.PromoPlanID
	, ppp.ProductID
	, ppp.DistrBranchID
	, pp.BeginDeliveryDate
	, pp.EndDeliveryDate
	, ppp.Data+ppp1.Data+isNull(ppp2.Data,0)+isNull(ppp3.Data,0)+isNull(ppp4.Data,0) as Data
	, ppp.BeginDate
	, ppp.EndDate
	, isNull(ppp4.EndDate, isNull(ppp3.EndDate, isNull(ppp2.EndDate, ppp1.EndDate))) as FinishDate
	
from [core].[PromoPlanProduct] ppp--крайние недели
	inner join [core].[PromoPlan] pp
		on ppp.PromoPlanID = pp.ID
	inner join [core].[PromoPlanProduct] ppp1
		on ppp.PromoPlanID=ppp1.PromoPlanID
			and ppp.ProductID=ppp1.ProductID
				and ppp.DistrBranchID=ppp1.DistrBranchID
					and dateadd(day,1,ppp.EndDate)=ppp1.BeginDate 
	left join [core].[PromoPlanProduct] ppp2
		on ppp.PromoPlanID=ppp2.PromoPlanID
			and ppp.ProductID=ppp2.ProductID
				and ppp.DistrBranchID=ppp2.DistrBranchID
					and dateadd(day,1,ppp1.EndDate)=ppp2.BeginDate 
	left join [core].[PromoPlanProduct] ppp3
		on ppp.PromoPlanID=ppp3.PromoPlanID
			and ppp.ProductID=ppp3.ProductID
				and ppp.DistrBranchID=ppp3.DistrBranchID
					and dateadd(day,1,ppp2.EndDate)=ppp3.BeginDate
	left join [core].[PromoPlanProduct] ppp4
		on ppp.PromoPlanID=ppp4.PromoPlanID
			and ppp.ProductID=ppp4.ProductID
				and ppp.DistrBranchID=ppp4.DistrBranchID
					and dateadd(day,1,ppp3.EndDate)=ppp4.BeginDate
						--and (ppp1.EndDate<pp.BeginDeliveryDate or ppp1.BeginDate>pp.EndDeliveryDate) 
where pp.BeginDeliveryDate between ppp.BeginDate and ppp.EndDate
		and pp.EndDeliveryDate between ppp.EndDate and ppp.EndDate
		and pp.CreatedDate>=N'20161001'
order by pp.EndDeliveryDate

---Объединение объемов у разбитых периодов слева
--update ppp
--	set ppp.Data = ppp.Data+ppp1.Data+isNull(ppp2.Data,0)+isNull(ppp3.Data,0)+isNull(ppp4.Data,0) as Data
select distinct
	ppp.PromoPlanID
	, ppp.ProductID
	, ppp.DistrBranchID
	, pp.BeginDeliveryDate
	, pp.EndDeliveryDate
	, ppp.Data as SimpleData
	, ppp.Data+ppp1.Data+isNull(ppp2.Data,0)+isNull(ppp3.Data,0)+isNull(ppp4.Data,0) as Data
	, ppp.BeginDate
	, ppp.EndDate
	, isNull(ppp4.BeginDate, isNull(ppp3.BeginDate, isNull(ppp2.BeginDate, ppp1.BeginDate))) as StartDate
from [core].[PromoPlanProduct] ppp--крайние недели
	inner join [core].[PromoPlan] pp
		on ppp.PromoPlanID = pp.ID
	inner join [core].[PromoPlanProduct] ppp1
		on ppp.PromoPlanID=ppp1.PromoPlanID
			and ppp.ProductID=ppp1.ProductID
				and ppp.DistrBranchID=ppp1.DistrBranchID
					and dateadd(day,-1,ppp.BeginDate)=ppp1.EndDate 
	left join [core].[PromoPlanProduct] ppp2
		on ppp.PromoPlanID=ppp2.PromoPlanID
			and ppp.ProductID=ppp2.ProductID
				and ppp.DistrBranchID=ppp2.DistrBranchID
					and dateadd(day,-1,ppp1.BeginDate)=ppp2.EndDate 
	left join [core].[PromoPlanProduct] ppp3
		on ppp.PromoPlanID=ppp3.PromoPlanID
			and ppp.ProductID=ppp3.ProductID
				and ppp.DistrBranchID=ppp3.DistrBranchID
					and dateadd(day,-1,ppp2.BeginDate)=ppp3.EndDate
	left join [core].[PromoPlanProduct] ppp4
		on ppp.PromoPlanID=ppp4.PromoPlanID
			and ppp.ProductID=ppp4.ProductID
				and ppp.DistrBranchID=ppp4.DistrBranchID
					and dateadd(day,-1,ppp3.BeginDate)=ppp4.EndDate
where pp.BeginDeliveryDate between ppp.BeginDate and ppp.EndDate
		and pp.EndDeliveryDate between ppp.EndDate and ppp.EndDate
		--and pp.ID= 51236
		and pp.CreatedDate>=N'20161001'
order by pp.EndDeliveryDate

--удаление объемов за периодом промо
--delete ppp
 select 
	ppp.PromoPlanID
	,ppp.ProductID
	, pp.BeginDeliveryDate
	, pp.EndDeliveryDate
	, ppp.Data
	, ppp.BeginDate
	, ppp.EndDate
from [core].[PromoPlanProduct] ppp
	inner join [core].[PromoPlan] pp
		on ppp.PromoPlanID = pp.ID
where (ppp.BeginDate>pp.EndDeliveryDate or ppp.EndDate<pp.BeginDeliveryDate) and (pp.BeginDeliveryDate<pp.EndDeliveryDate)
--and pp.CreatedDate>=N'20161001' 
order by pp.BeginDeliveryDate, ppp.ProductID, ppp.BeginDate



--update ppp
--	set ppp.BeginDate = case when ppp.BeginDate<pp.BeginDeliveryDate then pp.BeginDeliveryDate else ppp.BeginDate end
--	, ppp.EndDate = case when ppp.EndDate>pp.EndDeliveryDate then pp.EndDeliveryDate else ppp.EndDate end
select distinct
	ppp.PromoPlanID
	, ppp.ProductID
	, pp.BeginDeliveryDate
	, pp.EndDeliveryDate
	, ppp.BeginDate
	, ppp.EndDate
	, case when ppp.BeginDate<pp.BeginDeliveryDate then pp.BeginDeliveryDate else ppp.BeginDate end as NewBeginDate
	, case when ppp.EndDate>pp.EndDeliveryDate then pp.EndDeliveryDate else ppp.EndDate end as NewEndDate
	
from [core].[PromoPlanProduct] ppp
	inner join [core].[PromoPlan] pp
		on ppp.PromoPlanID = pp.ID
where (ppp.BeginDate<pp.BeginDeliveryDate or ppp.EndDate>pp.EndDeliveryDate)
order by pp.EndDeliveryDate


