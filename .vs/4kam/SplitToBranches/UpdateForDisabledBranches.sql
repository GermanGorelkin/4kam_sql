declare @amounts [api].[product_amount_tbltype]
declare @promoPlanID int, @networkID int, @createdUserID int


--[core].[PromoPlanProduct]
DECLARE my_cursor CURSOR FOR   
select distinct
	ppp.PromoPlanID
	, pp.NetworkID
	, max(ppp.CreatedUserID)
from 
[core].[PromoPlanProduct] ppp
inner join [core].[PromoPlan] pp
	on ppp.PromoPlanID=pp.ID
where
	pp.BeginShelfDate>N'01.11.2016'
	and ppp.DistrBranchID in (6057,6047,4837,4523,4844,4740,4739,4322)
group by ppp.PromoPlanID, pp.NetworkID
  
OPEN my_cursor  
FETCH NEXT FROM my_cursor   
INTO @promoPlanID, @networkID, @createdUserID
 
WHILE @@FETCH_STATUS = 0  
BEGIN 
	delete from @amounts

	insert into @amounts
	select
		ROW_NUMBER() over (order by ppp.ProductID)
		, ppp.ProductID
		, ppp.BeginDate
		, ppp.EndDate
		, sum(ppp.Data)
		, ppp.PromoPlanID
	from [core].[PromoPlanProduct] ppp
		inner join [core].[PromoPlan] pp
			on ppp.PromoPlanID = pp.ID
	where pp.ID=@promoPlanID
	group by
	ppp.ProductID, ppp.BeginDate, ppp.EndDate, ppp.PromoPlanID

	delete ppp 
	from [core].[PromoPlanProduct] ppp
	where ppp.PromoPlanID=@promoPlanID


	insert into [core].[PromoPlanProduct](ProductID, PromoPlanID, CreatedUserID, Data, BeginDate, EndDate, DistrBranchID)
	select
		ppp.ProductID
		, ppp.PromoPlanID
		, @createdUserID
		, ppp.Amount
		, case
			when ppp.BeginDate<pp.BeginDeliveryDate then pp.BeginDeliveryDate
			else ppp.BeginDate end as BeginDate
		, case
			when ppp.EndDate>pp.EndDeliveryDate then pp.EndDeliveryDate
			else ppp.EndDate end as EndDate
		, ppp.DistrBranchID 
	from 
		[api].[CreateNetworkBranchAmounts](@networkID, @amounts) ppp
		inner join [core].[PromoPlan] pp
			on ppp.PromoPlanID=pp.ID
	where ppp.Amount>0

	--insert into @diffs 
	--select
	--	t0.PromoPlanID
	--from
	--(select @promoPlanID as PromoPlanID, sum(Data) as Amount from [core].[PromoPlanProduct] where PromoPlanID=@promoPlanID) t0
	--inner join
	--(select @promoPlanID as PromoPlanID, sum(Amount) as Amount from [api].[CreateNetworkBranchAmounts](@networkID, @amounts) where Amount>0) t1
	--	on t0.PromoPlanID=t1.PromoPlanID and  t0.Amount!=t1.Amount


FETCH NEXT FROM my_cursor   
INTO @promoPlanID, @networkID, @createdUserID
END
CLOSE my_cursor;  
DEALLOCATE my_cursor;  
