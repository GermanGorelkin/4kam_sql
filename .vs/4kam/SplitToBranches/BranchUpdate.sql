
declare @amounts [api].[product_amount_tbltype]
declare @networkID int, @createdUserID int


--[core].[NoPromoPlanProduct]
DECLARE my_cursor CURSOR FOR   
select distinct
	NetworkID
	, CreatedUserID
from [core].[NoPromoPlanProduct]
where DistrBranchID is null
  
OPEN my_cursor  
FETCH NEXT FROM my_cursor   
INTO @networkID, @createdUserID 
  
WHILE @@FETCH_STATUS = 0  
BEGIN 
	delete from @amounts

	insert into @amounts
	select
		ID 
		, ProductID
		, BeginDate
		, EndDate
		, Data
		, null
	from [core].[NoPromoPlanProduct]
	where DistrBranchID is null and NetworkID = @networkID and CreatedUserID = @createdUserID 

	delete from [core].[NoPromoPlanProduct] where DistrBranchID is null and NetworkID = @networkID and CreatedUserID = @createdUserID 
	insert into [core].[NoPromoPlanProduct](ProductID, NetworkID, CreatedUserID, Data, BeginDate, EndDate, DistrBranchID)
	select
		ProductID
		, @networkID
		, @createdUserID
		, Amount
		, BeginDate
		, EndDate
		, DistrBranchID 
	from [api].[CreateNetworkBranchAmounts](@networkID, @amounts) where Amount>0

FETCH NEXT FROM my_cursor   
INTO @networkID, @createdUserID
END
CLOSE my_cursor;  
DEALLOCATE my_cursor;  


--[core].[PromoPlanProduct]
DECLARE my_cursor CURSOR FOR   
select distinct
	pp.NetworkID
	, ppp.CreatedUserID
from [core].[PromoPlanProduct] ppp
	inner join [core].[PromoPlan] pp
		on ppp.PromoPlanID = pp.ID
where ppp.DistrBranchID is null
  
OPEN my_cursor  
FETCH NEXT FROM my_cursor   
INTO @networkID, @createdUserID 
  
WHILE @@FETCH_STATUS = 0  
BEGIN 
	delete from @amounts

	insert into @amounts
	select
		ppp.ID 
		, ppp.ProductID
		, ppp.BeginDate
		, ppp.EndDate
		, ppp.Data
		, ppp.PromoPlanID
	from [core].[PromoPlanProduct] ppp
		inner join [core].[PromoPlan] pp
			on ppp.PromoPlanID = pp.ID
	where ppp.DistrBranchID is null and pp.NetworkID = @networkID and ppp.CreatedUserID = @createdUserID 
	and ppp.Data>0 	and ppp.ID not in (select
							t1.ID
						from [core].[PromoPlanProduct] t0
							inner join [core].[PromoPlanProduct] t1
								on t0.PromoPlanID=t1.PromoPlanID
									and t0.ProductID=t1.ProductID
										and t0.BeginDate>t1.BeginDate and t0.EndDate<t1.EndDate 
						group by t1.ID, t1.Data
						having sum(t0.Data)=t1.Data)

	delete ppp 
	from [core].[PromoPlanProduct] ppp
		inner join [core].[PromoPlan] pp
			on ppp.PromoPlanID = pp.ID
	where DistrBranchID is null 
	and pp.NetworkID = @networkID and ppp.CreatedUserID = @createdUserID 

	insert into [core].[PromoPlanProduct](ProductID, PromoPlanID, CreatedUserID, Data, BeginDate, EndDate, DistrBranchID)
	select
		ProductID
		, PromoPlanID
		, @createdUserID
		, Amount
		, BeginDate
		, EndDate
		, DistrBranchID 
	from [api].[CreateNetworkBranchAmounts](@networkID, @amounts) where Amount>0

FETCH NEXT FROM my_cursor   
INTO @networkID, @createdUserID
END
CLOSE my_cursor;  
DEALLOCATE my_cursor;  