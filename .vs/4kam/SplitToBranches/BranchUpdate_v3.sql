
declare @amounts [api].[product_amount_tbltype]
declare @networkID int, @createdUserID int


--[core].[NoPromoPlanProduct]
DECLARE my_cursor CURSOR FOR   
select distinct
	NetworkID
	, CreatedUserID
from [core].[NoPromoPlanProduct]
--where DistrBranchID is null
  
OPEN my_cursor  
FETCH NEXT FROM my_cursor   
INTO @networkID, @createdUserID 
  
WHILE @@FETCH_STATUS = 0  
BEGIN 
	delete from @amounts

	insert into @amounts
	select
		ID 
		, ProductID
		, BeginDate
		, EndDate
		, Data
		, null
	from [core].[NoPromoPlanProduct]
	where DistrBranchID is null and NetworkID = @networkID and CreatedUserID = @createdUserID 

	delete from [core].[NoPromoPlanProduct] where DistrBranchID is null and NetworkID = @networkID and CreatedUserID = @createdUserID 
	insert into [core].[NoPromoPlanProduct](ProductID, NetworkID, CreatedUserID, Data, BeginDate, EndDate, DistrBranchID)
	select
		ProductID
		, @networkID
		, @createdUserID
		, Amount
		, BeginDate
		, EndDate
		, DistrBranchID 
	from [api].[CreateNetworkBranchAmounts_2](@networkID, @amounts) where Amount>0

	--select 
	--@networkID as NetworkID
	--, *
	--from
	--(select PromoPlanID, sum(Amount) Amount from @amounts group by PromoPlanID) t0
	--inner join (select PromoPlanID, sum(Amount) as Amount from [api].[CreateNetworkBranchAmounts_2](@networkID, @amounts) where Amount>0 group by PromoPlanID)t1
	--	on t0.PromoPlanID= t1.PromoPlanID
	--where t0.Amount!=t1.Amount


FETCH NEXT FROM my_cursor   
INTO @networkID, @createdUserID
END
CLOSE my_cursor;  
DEALLOCATE my_cursor;  


