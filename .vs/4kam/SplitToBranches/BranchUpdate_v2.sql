declare @amounts [api].[product_amount_tbltype]
declare @networkID int, @createdUserID int


--[core].[PromoPlanProduct]
DECLARE my_cursor CURSOR FOR   
select distinct
	pp.NetworkID
	, ppp.CreatedUserID
from [core].[PromoPlanProduct] ppp
	inner join [core].[PromoPlan] pp
		on ppp.PromoPlanID = pp.ID
  
OPEN my_cursor  
FETCH NEXT FROM my_cursor   
INTO @networkID, @createdUserID 
  
WHILE @@FETCH_STATUS = 0  
BEGIN 
	delete from @amounts

	insert into @amounts
	select
		ROW_NUMBER() over (order by ppp.ProductID)
		, ppp.ProductID
		, ppp.BeginDate
		, ppp.EndDate
		, sum(ppp.Data)
		, ppp.PromoPlanID
	from [core].[PromoPlanProduct] ppp
		inner join [core].[PromoPlan] pp
			on ppp.PromoPlanID = pp.ID
	where pp.NetworkID = @networkID and ppp.CreatedUserID = @createdUserID 
	group by
	ppp.ProductID, ppp.BeginDate, ppp.EndDate, ppp.PromoPlanID

	delete ppp 
	from [core].[PromoPlanProduct] ppp
		inner join [core].[PromoPlan] pp
			on ppp.PromoPlanID = pp.ID
	where pp.NetworkID = @networkID and ppp.CreatedUserID = @createdUserID 

	insert into [core].[PromoPlanProduct](ProductID, PromoPlanID, CreatedUserID, Data, BeginDate, EndDate, DistrBranchID)
	select
		ProductID
		, PromoPlanID
		, @createdUserID
		, Amount
		, BeginDate
		, EndDate
		, DistrBranchID 
	from [api].[CreateNetworkBranchAmounts_2](@networkID, @amounts) where Amount>0

FETCH NEXT FROM my_cursor   
INTO @networkID, @createdUserID
END
CLOSE my_cursor;  
DEALLOCATE my_cursor;  