
--������� � �����
update t0
	set t0.Data = t0.Data+t1.Data
from [core].[PromoPlanProduct] t0
inner join 
(select
	pp.ID,
	sum(nopromo.Data) as Data
from 
	[core].[PromoPlan] promo
	inner join [core].[PromoPlanPFormat] promoFormat
		on promo.ID = promoFormat.PromoPlanID
	inner join [taxonomy].[Product] product
		on product.FormatID=promoFormat.ProductFormatID
	inner join [core].[NoPromoPlanProduct] nopromo
		on nopromo.NetworkID = promo.NetworkID
			and product.ID=nopromo.ProductID
			and promo.BeginDeliveryDate<=nopromo.EndDate
			and promo.EndDeliveryDate>=nopromo.BeginDate
	inner join [core].[PromoPlanProduct] pp
		on pp.PromoPlanID=promo.ID
			and pp.ProductID=nopromo.ProductID
			and pp.DistrBranchID=nopromo.DistrBranchID
			and pp.BeginDate>=nopromo.BeginDate
			and pp.EndDate<=nopromo.EndDate
where
	promoFormat.Active=1
	and promo.Active=1
	and isNull(promo.IsDel,0)=0
	and nopromo.Data>0
group by pp.ID) t1
on t0.ID=t1.ID

------�������� �������, ������� ���� ��������� � ����� 
delete nopromo
from 
	[core].[PromoPlan] promo
	inner join [core].[PromoPlanPFormat] promoFormat
		on promo.ID = promoFormat.PromoPlanID
	inner join [taxonomy].[Product] product
		on product.FormatID=promoFormat.ProductFormatID
	inner join [core].[NoPromoPlanProduct] nopromo
		on nopromo.NetworkID = promo.NetworkID
			and product.ID=nopromo.ProductID
			and promo.BeginDeliveryDate<=nopromo.EndDate
			and promo.EndDeliveryDate>=nopromo.BeginDate
	inner join [core].[PromoPlanProduct] pp
		on pp.PromoPlanID=promo.ID
			and pp.ProductID=nopromo.ProductID
			and pp.DistrBranchID=nopromo.DistrBranchID
			and pp.BeginDate>=nopromo.BeginDate
			and pp.EndDate<=nopromo.EndDate
where
	promoFormat.Active=1
	and promo.Active=1
	and isNull(promo.IsDel,0)=0

--��������� ����� 
insert into [core].[PromoPlanProduct](PromoPlanID, ProductID, Data, CreatedUserID, BeginDate, EndDate, DistrBranchID)
select distinct
	promo.ID
	, nopromo.ProductID
	, sum(nopromo.Data) as Data
	, promo.CreatedUserID
	, nopromo.BeginDate
	, nopromo.EndDate
	, nopromo.DistrBranchID
	--, promo.BeginDeliveryDate
	--, promo.EndDeliveryDate
--delete nopromo
from 
	[core].[PromoPlan] promo
	inner join [core].[PromoPlanPFormat] promoFormat
		on promo.ID = promoFormat.PromoPlanID
	inner join [taxonomy].[Product] product
		on product.FormatID=promoFormat.ProductFormatID
	inner join [core].[NoPromoPlanProduct] nopromo
		on nopromo.NetworkID = promo.NetworkID
			and product.ID=nopromo.ProductID
			and promo.BeginDeliveryDate<=nopromo.EndDate
			and promo.EndDeliveryDate>=nopromo.BeginDate
where
	promoFormat.Active=1
	and promo.Active=1
	and isNull(promo.IsDel,0)=0
group by promo.ID, nopromo.ProductID, promo.CreatedUserID, nopromo.BeginDate, nopromo.EndDate, nopromo.DistrBranchID
having sum(nopromo.Data)>0



delete nopromo
from 
	[core].[PromoPlan] promo
	inner join [core].[PromoPlanPFormat] promoFormat
		on promo.ID = promoFormat.PromoPlanID
	inner join [taxonomy].[Product] product
		on product.FormatID=promoFormat.ProductFormatID
	inner join [core].[NoPromoPlanProduct] nopromo
		on nopromo.NetworkID = promo.NetworkID
			and product.ID=nopromo.ProductID
			and promo.BeginDeliveryDate<=nopromo.EndDate
			and promo.EndDeliveryDate>=nopromo.BeginDate
where
	promoFormat.Active=1
	and promo.Active=1
	and isNull(promo.IsDel,0)=0