--update t0
--	set Data= sum(Data)
select *
from [core].[PromoPlanProduct] t0
inner join
(SELECT 
   max(ID) as ID
  , ppp.[PromoPlanID]
  , ppp.[ProductID]
  , sum(Data) as Data
  , max(CreatedUserID) as CreatedUserID
  , ppp.[BeginDate]
  , ppp.[EndDate]
  , ppp.DistrBranchID
FROM [core].[PromoPlanProduct] ppp
group by [ProductID]
      ,ppp.[BeginDate]
      ,ppp.[EndDate]
   ,ppp.DistrBranchID
   ,ppp.[PromoPlanID]
having count(*)>1) t1
	on t0.ID=t1.ID

--delete t0
select *
from [core].[PromoPlanProduct] t0
inner join
(SELECT 
   max(ID) as ID
  , ppp.[PromoPlanID]
  , ppp.[ProductID]
  , sum(Data) as Data
  , max(CreatedUserID) as CreatedUserID
  , ppp.[BeginDate]
  , ppp.[EndDate]
  , ppp.DistrBranchID
FROM [core].[PromoPlanProduct] ppp
group by [ProductID]
      ,ppp.[BeginDate]
      ,ppp.[EndDate]
   ,ppp.DistrBranchID
   ,ppp.[PromoPlanID]
having count(*)>1) t1
	on t0.PromoPlanID=t1.PromoPlanID
		and t0.ProductID=t1.ProductID
			and t0.BeginDate=t1.BeginDate
			and t0.EndDate=t1.EndDate
			and t0.DistrBranchID=t1.DistrBranchID
	and t0.ID<t1.ID



