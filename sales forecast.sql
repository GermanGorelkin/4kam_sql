﻿USE [4kam_promo]
GO

SELECT [BeginShelfDate]
      ,[EndShelfDate]
      ,[BeginDeliveryDate]
      ,[EndDeliveryDate]
      ,[PromoTypeID]
      ,[PromoStatusID]
      ,n.[name] as Network
      ,[CreatedDate]
      ,[Comment]
   ,pf.[name] as 'Promo format'
   ,pm.Discount as 'PPD'
   ,p.NameEN
   ,p.EAN
   ,sum(ppp.[Data]) as [Plan]
   ,distr.[name] as 'Distributors'
   ,branch.[name] as 'Branch'
   ,branch.City
   ,region.[name] as 'Region'
   ,pp.ID
  FROM [core].[PromoPlan] pp
  join [core].[PromoPlanPFormat] pfp on 
 pfp.PromoPlanID=pp.ID
  join [dbo].[Networks] n on
 n.id=pp.[NetworkID]
  join [dbo].[ProductFormat] pf on
 pf.id=pfp.[ProductFormatID]
  left join [core].[PromoPlanMechanic] pm on 
 pm.PromoPlanID=pp.ID
  left join [core].[PromoPlanProduct] ppp on 
 ppp.PromoPlanID=pp.ID 
 and ppp.[Data]>0 
  left join [taxonomy].[Product] p on
 p.ID=ppp.[ProductID]
  left join [dbo].[DistributorBranch] db on
 db.id=ppp.DistrBranchID
  left join [dbo].[Distributor] distr on 
 distr.id=db.distributorID
  left join [dbo].[Branch] branch on
 branch.id=db.branchID
  left join [dbo].[Territorys] region on
 region.id=db.[regionID]
  where pp.[Active]=1 and pp.[IsDel]=0
  and pp.[NetworkID] in (309) 
  and [BeginDeliveryDate]>='01.01.2016'
  group by [BeginShelfDate],[EndShelfDate],[BeginDeliveryDate],
  [EndDeliveryDate],[PromoTypeID],[PromoStatusID],
   n.[name],[CreatedDate],[Comment],
   pf.[name],pm.Discount,p.NameEN,p.EAN,distr.[name],
   branch.[name],branch.City,region.[name]
    ,pp.ID